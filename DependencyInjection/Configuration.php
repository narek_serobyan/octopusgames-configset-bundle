<?php

namespace OctopusGames\ConfigsetBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use OctopusGames\ConfigsetBundle\Document;
use OctopusGames\ConfigsetBundle\Entity;
use OctopusGames\ConfigsetBundle\Form;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('octopus_games_configset');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $this->addConfigsetSection($rootNode);
        $this->addGridSection($rootNode);


        return $treeBuilder;
    }

    private function addConfigsetSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->scalarNode('driver')->defaultValue('orm')->cannotBeEmpty()->end()
                ->arrayNode('configset')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                        ->children()
                            ->scalarNode('upload_env')->defaultValue('dev')->cannotBeEmpty()->end()
                            ->scalarNode('entityClass')->defaultValue(Entity\Configset::class)->cannotBeEmpty()->end()
                            ->scalarNode('documentClass')->defaultValue(Document\Configset::class)->cannotBeEmpty()->end()
                            ->scalarNode('upload_dir')->cannotBeEmpty()->end()
                            ->arrayNode('form')
                                ->addDefaultsIfNotSet()
                                ->fixXmlConfig('validation_group')
                                    ->children()
                                        ->scalarNode('type')->defaultValue(Form\Configset\ConfigsetType::class)->end()
                                        ->scalarNode('name')->defaultValue('configset_form')->end()
                                        ->arrayNode('validation_groups')
                                            ->prototype('scalar')->end()
                                            ->defaultValue(['Configset', 'Default'])
                                        ->end()
                                    ->end()
                            ->end()
                        ->end()
                ->end()
            ->end();
    }

    private function addGridSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->scalarNode('grid_max_per_page')->defaultValue(10)->cannotBeEmpty()->end()
            ->end();
    }
}
