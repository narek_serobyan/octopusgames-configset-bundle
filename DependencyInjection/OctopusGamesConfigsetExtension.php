<?php

namespace OctopusGames\ConfigsetBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class OctopusGamesConfigsetExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config/services'));

        foreach (array('services', 'configset') as $basename) {
            $loader->load(sprintf('%s.yml', $basename));
        }

        $this->setConfigsetParams($config, $container);
        $this->setGridParams($config, $container);
    }

    private function setConfigsetParams(array $config, ContainerBuilder $container)
    {
        $container->setParameter('driver', $config['driver']);
        $document = $config['driver'] == 'orm' ? $config['configset']['entityClass'] : $config['configset']['documentClass'];
        $container->setParameter('configsetDocument', $document);
        $container->setParameter('configsetName', $config['configset']['form']['name']);
        $container->setParameter('configsetType', $config['configset']['form']['type']);
        $container->setParameter('configsetValidationGroup', $config['configset']['form']['validation_groups']);
        $container->setParameter('configsetUploadDir', $config['configset']['upload_dir']);
        $container->setParameter('configsetUploadEnv', $config['configset']['upload_env']);
    }

    private function setGridParams(array $config, ContainerBuilder $container)
    {
        $container->setParameter('maxPerPage', $config['grid_max_per_page']);
    }
}
