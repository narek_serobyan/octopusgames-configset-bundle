<?php

namespace OctopusGames\ConfigsetBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ConfigsetEntityRepository extends EntityRepository
{
    public function findAllFiltered($columns)
    {
        return $this->createQueryBuilder('r')
            ->select('r.' . implode(',r.', $columns))
            ->getQuery()
            ->getResult();
    }

    public function getConfigsets($key, $param)
    {
        return $this->getConfigset([$key => $param], 'AND');
    }

    public function getConfigset($params = [], $operator = 'OR', $singleResult = false)
    {
        $sql = 'SELECT * FROM configset ';

        for ($i = 1; $i <= count($params); $i++) {
            $sql .= ($i == 1) ? ' WHERE attributes@> ? ' : $operator . '  attributes@> ? ';
        }

        $qb = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);

        $bindValue = 1;
        foreach ($params as $key => $param) {
            $param = json_encode([$key => $param]);

            $qb->bindValue($bindValue, $param);
            $bindValue++;
        }
        $qb->execute();

        if ($singleResult) {
            return $qb->fetch();
        }

        return $qb->fetchAll();
    }
}
