require('../../css/admin/config.sass')
const $ = require('jquery');
exports.jQuery = $;


$(document).ready(function() {

    let json = $('#json-data').data('json')
    $('#json-container').html('<pre>' + JSON.stringify(json, null, 4) + '</pre>')

    $('.navigate-list-button').click(function() {

        window.location.href = $(this).data('url')
    })


    var uploadButton = function() {
        $('.navigate-upload-button').click(function() {

            window.location.href = $(this).data('url')
        })
    }
    uploadButton()

    var viewConfigButton = function() {
        $('.view-config-button').click(function() {

            let url = $(this).data('url') + '?id=' + encodeURIComponent($(this).data('id'))
            window.location.href = url
        })
    }
    viewConfigButton()

    var backupConfigButton = function() {
        $('.backup-config-button').click(function() {

            $.ajax({
                type: 'POST',
                url: $(this).data('url'),
                data: {
                    id: $(this).data('id')
                },
                dataType: 'json',
                success: function(result) {

                    $('.body-container').html(result.update)
                    subscribeHandlers()
                }
            })
        })
    }
    backupConfigButton()

    var removeConfigButton = function() {
        $('.remove-config-button').click(function() {

            $.ajax({
                type: 'POST',
                url: $(this).data('url'),
                data: {
                    id: $(this).data('id')
                },
                dataType: 'json',
                success: function(result) {

                    $('.body-container').html(result.update)
                    subscribeHandlers()
                }
            })
        })
    }
    removeConfigButton()

    var subscribeHandlers = function() {

        uploadButton()
        viewConfigButton()
        backupConfigButton()
        removeConfigButton()
    }
})
