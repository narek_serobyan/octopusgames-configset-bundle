Configset bundle
=======================

This bundle provides functionality for uploading and downloading configs 
as well as saving the history of uploading and merge configs between servers. 
The package also provides the ability to customize configs, it is possible to add new fields, change the forms.
All this is done by adding settings to the configuration of the project. 
It is also possible to configure the admin part of which fields to view. 
Everything is configured through annotations.

Installation
------------

- Step1:
Download ConfigsetBundle using composer
Require the bundle with composer:
``` bash
$> composer require octopusgames/configset-bundle 
``` 
Composer will install the bundle to your project's vendor/octopusgames/configset-bundle directory. 
- Step2:
Enable the bundle
Enable the bundle in the kernel:
```php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new OctopusGames\ConfigsetBundle\OctopusGamesConfigsetBundle(),
        // ...
    );
}
```

- Step3:
Create your configset class
The goal of this bundle is to persist some configset class to a database (MySql, MongoDB, CouchDB, etc). 

The bundle provides base classes which are already mapped for most fields to make it easier to create your entity. 
If you want customize default fields, you must create your own entity class.
Here is how you use it:
```php
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OctopusGames\ConfigsetBundle\Entity\Model\ConfigsetBase;
use OctopusGames\ConfigsetBundle\Annotation as Octo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="configset")
 * @ORM\Entity(
 *     repositoryClass="OctopusGames\ConfigsetBundle\Repository\ConfigsetEntityRepository"
 * )
 * @Octo\Grid(columns="id, comment, attributes, filename, createdAt")
 * @Octo\History(columns="comment, attributes, filename, clientConfigs, serverConfigs, historyPath")
 */
class Configset extends ConfigsetBase
{
}

```
And configure your config.yml
```yaml
octopus_games_configset:
    configset:
        entityClass: AppBundle\Entity\Configset

```
-Step4:
Configure your application's config.yml<br />
setup upload path
```yaml
octopus_games_configset:
    configset:
        upload_dir: '%kernel.project_dir%/var/upload/'

```
setup upload env, there uploaded configs will saved 
```yaml
octopus_games_configset:
    configset:
        upload_env: 'test'

```
-Step5:
Import ConfigsetBundle routing files 
```yaml
octopusgames_configset:
  resource: '@OctopusGamesConfigsetBundle/Resources/config/routing/all.yml'

```

-Step6:
Update your database schema
```bash
$ php bin/console doctrine:schema:update --force
```

-Step7:
Assets Install 
```bash
$ php bin/console assets:install web
```


Customization
------------

```yaml
octopus_games_configset:
    grid_max_per_page: 10
    configset:
      upload_env: test
      entityClass: AppBundle\Entity\Configset
      documentClass: AppBundle\Document\Configset
      upload_dir: '%kernel.project_dir%/var/upload/'
      form:
          type: AppBundle\Form\Bundleset\BundlesetType
```

- grid_max_per_page:
limit of items in page for grid, default value is 10
- configset->upload_env: 
uploaded config saved to this env, default value is dev
- configset->entityClass: 
customize class for enity configset
- configset->upload_dir: 
path there saved uploaded configs and history 
- configset->form: 
customize form for configset


- How to customize grid view <br />
use this annotation in entity class
```php
use OctopusGames\ConfigsetBundle\Annotation as Octo;

@Octo\Grid(columns="id, comment, attributes, filename, createdAt")
```

- How to customize history fields <br />
use this annotation in entity class
```php
use OctopusGames\ConfigsetBundle\Annotation as Octo;

@Octo\History(columns="comment, attributes, filename, clientConfigs, serverConfigs, historyPath")
```
