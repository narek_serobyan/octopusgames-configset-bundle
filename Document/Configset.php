<?php

namespace OctopusGames\ConfigsetBundle\Document;


use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\HttpFoundation\File\File;
use OctopusGames\ConfigsetBundle\Document\Model\ConfigsetBase;
use OctopusGames\ConfigsetBundle\Document\ConfigsetEmbed\Config;
use Doctrine\Common\Collections\ArrayCollection;
use OctopusGames\ConfigsetBundle\Annotation as Octo;

/**
 * @ODM\Document(collection="configset")
 * @Octo\Grid(columns="id, displayName, description, filename, createdAt")
 */
class Configset extends ConfigsetBase
{
}
