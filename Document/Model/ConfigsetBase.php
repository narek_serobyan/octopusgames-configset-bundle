<?php

namespace OctopusGames\ConfigsetBundle\Document\Model;

use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\MappedSuperclass;
use OctopusGames\ConfigsetBundle\Document\ConfigsetEmbed\Config;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Events;
use Symfony\Component\Filesystem\Filesystem;
use OctopusGames\ConfigsetBundle\Annotation as Octo;

/**
 * @MappedSuperclass
 * @ODM\HasLifecycleCallbacks
 *
 */
class ConfigsetBase
{
    /**
     * @ODM\Id(strategy="increment")
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     * @Octo\GridColumn(title="Display Name", type="string", filterable=true)
     */
    private $displayName;

    /**
     * @ODM\Field(type="string")
     * @Octo\GridColumn(title="Description", type="string", filterable=true)
     */
    private $description;

    /**
     * @ODM\Field(type="hash")
     */
    private $attributes;

    /**
     * @ODM\Field(type="date")
     * @Octo\GridColumn(title="Description", type="date", filterable=true)
     */
    private $createdAt;

    /**
     * @ODM\EmbedMany(
     *     targetDocument="\OctopusGames\ConfigsetBundle\Document\ConfigsetEmbed\Config",
     *     strategy="addToSet"
     * )
     */
    protected $clientConfigs;

    /**
     * @ODM\EmbedMany(
     *     targetDocument="\OctopusGames\ConfigsetBundle\Document\ConfigsetEmbed\Config",
     * )
     */
    protected $serverConfigs;

    /**
     * @ODM\Field(type="string")
     * @Octo\GridColumn(title="Filename", type="file", filterable=false)
     */
    private $filename;

    /**
     * @ODM\Field(type="string")
     */
    private $historyPath;

    /**
     */
    private $file;

    public function __construct()
    {
        $this->clientConfigs = new ArrayCollection();
        $this->serverConfigs = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     * @return $this
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string $displayName
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set attributes
     *
     * @param hash $attributes
     * @return $this
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Get attributes
     *
     * @return hash $attributes
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set createdAt
     * @ODM\PrePersist
     * @param date $createdAt
     * @return $this
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return date $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add config
     *
     * @param Config $config
     */
    public function addClientConfig(Config $config)
    {
        $this->clientConfigs[] = $config;
    }

    /**
     * Remove config
     *
     * @param Config $config
     */
    public function removeClientConfig(Config $config)
    {
        $this->clientConfigs->removeElement($config);
    }

    /**
     * Get configs
     *
     * @return \Doctrine\Common\Collections\Collection $configs
     */
    public function getClientConfigs()
    {
        return $this->clientConfigs;
    }

    /**
     * Add config
     *
     * @param Config $config
     */
    public function addServerConfig(Config $config)
    {
        $this->serverConfigs[] = $config;
    }

    /**
     * Remove config
     *
     * @param Config $config
     */
    public function removeServerConfig(Config $config)
    {
        $this->serverConfigs->removeElement($config);
    }

    /**
     * Get configs
     *
     * @return \Doctrine\Common\Collections\Collection $configs
     */
    public function getServerConfigs()
    {
        return $this->serverConfigs;
    }

    /**
     * Set fileName
     *
     * @param custom_id $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->filename = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return custom_id $fileName
     */
    public function getFileName()
    {
        return $this->filename;
    }

    /**
     * Set historyPath
     *
     * @param historyPath $historyPath
     * @return $this
     */
    public function setHistoryPath($historyPath)
    {
        $this->historyPath = $historyPath;

        return $this;
    }

    /**
     * Get historyPath
     *
     * @return historyPath $historyPath
     */
    public function getHistoryPath()
    {
        return $this->historyPath;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ODM\PreRemove
     */
    public function deleteHistory()
    {
        (new Filesystem())->remove($this->getHistoryPath());
    }
}
