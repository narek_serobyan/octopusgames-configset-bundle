<?php

namespace OctopusGames\ConfigsetBundle\Document\ConfigsetEmbed;

use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\MappedSuperclass;

/**
 * @MappedSuperclass
 * @ODM\HasLifecycleCallbacks
 *
 */
class Config
{
    /**
     * @ODM\Field(type="string")
     * @ODM\Id(strategy="NONE")
     * @Assert\NotBlank(groups={"view"})
     */
    private $fileName = null;

    /**
     * @ODM\Field(type="string")
     */
    private $json = null;

    /**
     * @Assert\File(
        mimeTypes = {"application/json", "text/plain"},
        mimeTypesMessage = "Please upload a valid JSON"
        )
     * @Assert\NotBlank()
     */
    private $file = null;

    /**
     * @ODM\Field(type="date")
     */
    private $createdAt = null;

    /**
     * Set fileName
     *
     * @param custom_id $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return custom_id $fileName
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set json
     *
     * @param string $json
     * @return $this
     */
    public function setJson($json)
    {
        $this->json = $json;

        return $this;
    }

    /**
     * Get json
     *
     * @return string $json
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set createdAt
     * @ODM\PrePersist
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return string $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
