<?php

namespace OctopusGames\ConfigsetBundle\Form\Configset;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use OctopusGames\ConfigsetBundle\File\File;
use Symfony\Component\Filesystem\Filesystem;
use OctopusGames\ConfigsetBundle\File\File as OctoFile;

class ConfigsetType extends AbstractType
{
    private $class;
    private $manager;
    private $uploadDir;
    private $sendData = [];

    /**
     * BundlesetType constructor.
     * @param $class
     * @param $uploadDir
     */
    public function __construct($manager, $class, $uploadDir)
    {
        $this->manager = $manager;
        $this->class = $class;
        $this->uploadDir = $uploadDir;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', TextType::class)
            ->add('attributes', TextType::class)
            ->add('filename', TextType::class)
            ->add('clientConfigsFile', FileType::class, ['mapped' => false])
            ->add('serverConfigsFile', FileType::class, ['mapped' => false])
            ->add('author', TextType::class, ['mapped' => false])
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                [$this, 'onPostSubmit']
            )
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                [$this, 'onPreSubmit']
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => $this->class,
                'csrf_protection' => false,
            ]
        );
    }

    public function onPreSubmit(FormEvent $event)
    {
        $this->sendData = $event->getData();
    }

    public function onPostSubmit(FormEvent $event)
    {
        $configSet = $event->getData();

        $configSet->setAttributes(json_decode($configSet->getAttributes(), true));

        if (!empty($this->sendData['clientConfigsFile'])) {
            $this->setConfigs($this->sendData['clientConfigsFile'], $configSet, 'client');
        }

        if (!empty($this->sendData['serverConfigsFile'])) {
            $this->setConfigs($this->sendData['serverConfigsFile'], $configSet, 'server');
        }

        if (!empty($this->sendData['filename'])) {
            $file = File::saveFile($this->sendData['filename'], $this->uploadDir . 'csv/' . time());
            $configSet->setFileName($file['path']);
        }

        $event->setData($configSet);
    }


    private function setConfigs($configs, $configSet, $type)
    {
        $clientConfigs = $serverConfigs = [];
        $path = $this->uploadDir . 'config/' . time() . '/' . $type . 'Configs';

        foreach ($configs as $configFile) {
            $file = File::saveFile($configFile, $path);
            $config = [
                'name' => $file['fileName'],
                'path' => $file['path'],
                'md5' => md5_file($file['path'])
            ];

            if ($type == 'client') {
                $clientConfigs[] = $config;
            } else {
                $serverConfigs[] = $config;
            }
        }

        if ($type == 'client') {
            $this->removeOldFiles($configSet->getClientConfigs());
            $configSet->setClientConfigs($clientConfigs);
        } else {
            $this->removeOldFiles($configSet->getServerConfigs());
            $configSet->setServerConfigs($serverConfigs);
        }
    }

    private function removeOldFiles($configs)
    {
        foreach ($configs as $config) {
            $path = OctoFile::getBasePath($config['path'], 3);
            (new Filesystem())->remove($path);
            break;
        }
    }

    public function getName()
    {
        return '';
    }
}
