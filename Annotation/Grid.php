<?php

namespace OctopusGames\ConfigsetBundle\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
class Grid extends BaseAnnotation
{
    protected $filterable;
    protected $sortable;
    protected $groupBy;

    public function __construct($metadata = [])
    {
        parent::__construct($metadata);

        $this->filterable = isset($metadata['filterable']) ? $metadata['filterable'] : true;
        $this->sortable = isset($metadata['sortable']) ? $metadata['sortable'] : true;
        $this->groupBy = (isset($metadata['groupBy']) && $metadata['groupBy'] != '') ? (array) $metadata['groupBy'] : [];
    }

    public function isFilterable()
    {
        return $this->filterable;
    }

    public function isSortable()
    {
        return $this->sortable;
    }

    public function getGroupBy()
    {
        return $this->groupBy;
    }
}
