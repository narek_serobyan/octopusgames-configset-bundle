<?php

namespace OctopusGames\ConfigsetBundle\Annotation;

class BaseAnnotation
{
    protected $columns;
    protected $groups;

    public function __construct(array $metadata = [])
    {
        $this->columns = (isset($metadata['columns']) && $metadata['columns'] != '') ? array_map('trim', explode(',', $metadata['columns'])) : [];
        $this->groups = (isset($metadata['groups']) && $metadata['groups'] != '') ? (array) $metadata['groups'] : ['default'];
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function hasColumns()
    {
        return !empty($this->columns);
    }

    public function getGroups()
    {
        return $this->groups;
    }
}
