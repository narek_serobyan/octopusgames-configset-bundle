<?php

namespace OctopusGames\ConfigsetBundle\Annotation;

class BaseColumn
{
    protected $metadata;
    protected $groups;

    public function __construct($metadata)
    {
        $this->metadata = $metadata;
        $this->groups = isset($metadata['groups']) ? (array) $metadata['groups'] : ['default'];
    }

    public function getMetadata()
    {
        return $this->metadata;
    }

    public function getGroups()
    {
        return $this->groups;
    }
}
