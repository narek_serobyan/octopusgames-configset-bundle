<?php

namespace OctopusGames\ConfigsetBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use OctopusGames\ConfigsetBundle\Metadata\MetadataFactory;
use OctopusGames\ConfigsetBundle\Git\Git;
use OctopusGames\ConfigsetBundle\Factory\CommonFactory;
use OctopusGames\ConfigsetBundle\File\Json;

class HistoryManager
{
    use ContainerAwareTrait;

    private $class;

    public function __construct(MetadataFactory $metadata, $class)
    {
        $this->metadata = $metadata;
        $this->class = $class;
    }

    private function getGitRepo($object)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        return new Git($object, $user);
    }

    private function getBranch($branch)
    {
        $env = $this->container->getParameter('configsetUploadEnv');

        return ($branch == $env || !$branch) ? 'master' : $branch;
    }

    public function saveHistory($object, $params)
    {
        $git = $this->getGitRepo($object);
        $dir = $this->getDir($object, $git);

        $this->transformToFiles($object, $dir);

        $connections = $this->container->getParameter('doctrine.connections');

        $git->gitInit($dir)
            ->checkout()
            ->commit($object, $params, $connections);

        if ($git->getGitInit()) {
            $object->setHistoryPath($dir);
        }
    }


    private function getDir($object, $git)
    {
        if ($git->getGitInit()) {
            return $this->container->getParameter('configsetUploadDir') . 'config_history/' . time();
        } else {
            return $object->getHistoryPath();
        }
    }

    private function transformToFiles($object, $dir)
    {
        $fileSystem = new Filesystem();
        $metadata = $this->metadata->getMetadata(get_class($object));

        try {
            $fileSystem->mkdir($dir);
            foreach ($metadata as $field => $fieldParam) {
                if ($fieldParam['type'] == 'array') {
                    array_map('unlink', glob($dir . '/' . $field . '/*'));
                    foreach (call_user_func([$object, 'get' . ucfirst($field)]) as $config) {
                        $fileSystem->copy(
                            $config['path'],
                            $dir . '/' . $field . '/' . $config['name'],
                            true
                        );
                    }
                } elseif ($fieldParam['type'] == 'file') {
                    array_map('unlink', glob($dir . '/csv/*'));
                    $fileSystem->copy($object->getFilename(), $dir . '/csv/' . basename($object->getFilename()), true);
                } elseif ($fieldParam['type'] == 'string') {
                    $fileSystem->dumpFile($dir . '/' .$field, call_user_func([$object, 'get' . ucfirst($field)]));
                } elseif ($fieldParam['type'] == 'string_array') {
                    $fileSystem->dumpFile($dir . '/' .$field, serialize(call_user_func([$object, 'get' . ucfirst($field)])));
                }
            }
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at " . $exception->getPath();
        }

        return $this;
    }

    private function getBasePath($val, $type)
    {
        $uploadDir = $this->container->getParameter('configsetUploadDir');

        if ($val) {
            $val = explode('/', $val);
            array_pop($val);
            $basePath = implode('/', $val);
        } else {
            $basePath = $uploadDir . '/' . $type . '/' . time();
        }

        return $basePath . '/';
    }

    private function transformFilesToDbData($dir, $env)
    {
        $metadata = $this->metadata->getMetadata($this->class);
        $fileSystem = new Filesystem();

        $attributes = unserialize(file_get_contents($dir . '/attributes'));

        $configsetManager = $this->container->get('octopusgames_configset.configset_manager');
        $configsetManager->setEnv($env);
        $object = $configsetManager->getConfigsetByAttr($attributes);

        if (!$object) {
            $object = new $this->class;
        }

        try {
            foreach ($metadata as $field => $fieldParam) {
                if ($fieldParam['type'] == 'array') {
                    $val = call_user_func([$object, 'get' . ucfirst($field)]);
                    if (count($val)) {
                        $val = $val[0]['path'];
                    }
                    $param = [];
                    foreach(glob($dir . '/' . $field . '/*') as $file) {
                        $path = $this->getBasePath($val, 'config') . $field . '/' . basename($file);
                        $fileSystem->copy($file, $path, true);
                        $param[] = [
                            'name' => basename($file),
                            'path' => $path,
                            'md5' => md5_file($path)
                        ];
                    }
                } elseif ($fieldParam['type'] == 'file') {
                    $val = call_user_func([$object, 'get' . ucfirst($field)]);
                    foreach(glob($dir . '/csv/*') as $file) {
                        $param = $this->getBasePath($val, 'csv') . basename($file);
                        $fileSystem->copy($file, $param, true);
                    }
                } elseif ($fieldParam['type'] == 'string') {
                    $param = file_get_contents($dir . '/' . $field);
                } elseif ($fieldParam['type'] == 'string_array') {
                    $param = unserialize(file_get_contents($dir . '/' . $field));
                }
                call_user_func([$object, 'set' . ucfirst($field)], $param);
            }
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at " . $exception->getPath();
        }

        $configsetManager->persist($object, true);

        return $this;
    }

    public function getHistory($object, $env = null)
    {
        $data = [];

        $branch = $this->getBranch($env);

        $commits = $this->getGitRepo($object)->getCommits($branch);

        foreach ($commits as $commit) {
            $data[] = [
                'revision' => $commit->getRevision(),
                'author' => $commit->getAuthorEmail(),
                'date' => $commit->getCommitterDate(),
                'msg' => $commit->getMessage(),
                'diff' => $commit->getDiff(),
            ];
        }

        return $data;
    }

    public function mergeConfig($object, $from, $to, $comment)
    {
        $git = $this->getGitRepo($object);

        $from = $this->getBranch($from);

        if ($git->hasBranch($from) and !$git->hasBranch($to)) {
            $git->getWC()->checkout($from, $to);
        } else {
            $git->getWC()->checkout($to);
            $git->merge($from, $comment);
        }

        $this->transformFilesToDbData($object->getHistoryPath(), $to);

        return $this;
    }

    public function getConfig($object, $env, $commit)
    {
        $branch = $this->getBranch($env);

        $git = $this->getGitRepo($object);
        $git->getWC()->checkout($branch);
        $git->getWC()->checkout($commit);

        $files = [];
        foreach(glob($object->getHistoryPath() . '/clientConfigs/*') as $file) {
            $files[] = $file;
        }

        $file = CommonFactory::makeFile($files, $this->container->getParameter('configsetUploadDir'), Json::ZIP_FILE_TYPE);

        $git->getWC()->checkout($branch);

        return $file;
    }
}
