<?php

namespace OctopusGames\ConfigsetBundle\Manager;

use Doctrine\Common\Persistence\ManagerRegistry;

abstract class Manager implements ManagerInterface
{
    use UserAwareTrait;

    protected $env;
    protected $managerRegistry;
    protected $class;

    public function __construct(ManagerRegistry $managerRegistry, $class, $manager = 'orm')
    {
        $this->class = $class;
        $this->managerRegistry = $managerRegistry;
    }

    public function getManager()
    {
        return $this->managerRegistry->getManager($this->env);
    }

    public function setEnv($env)
    {
        return $this->env = $env;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepo()
    {
        return $this->getManager()->getRepository($this->class);
    }

    public function createObject()
    {
        $class = $this->getClass();

        return new $class();
    }

    /**
     * {@inheritdoc}
     */
    public function find(int $id)
    {
        return $this->getRepo()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function findObjectBy(array $criteria)
    {
        return $this->getRepo()->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findBy(array $criteria)
    {
        return $this->getRepo()->findBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
        return $this->getRepo()->findAll();
    }

    public function persist($object, $flush = false)
    {
        $this->getManager()->persist($object);

        if ($flush) {
            $this->flush($object);
        }
    }

    public function delete($object, $flush = false)
    {
        $this->getManager()->remove($object);

        if ($flush) {
            $this->flush($object);
        }
    }

    public function flush($object = null)
    {
        $this->getManager()->flush($object);
    }

    public function __call($name, $arguments)
    {
        return $this->getRepo()->$name(reset($arguments));
    }
}
