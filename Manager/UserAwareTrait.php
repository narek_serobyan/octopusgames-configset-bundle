<?php

namespace OctopusGames\ConfigsetBundle\Manager;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

trait UserAwareTrait
{
    /**
     * @var user
     */
    protected $user;

    /**
     * @required
     */
    public function setUser(TokenStorageInterface $tokenStorage)
    {
        if (!$tokenStorage->getToken()) {
            return null;
        }
        $this->user = $tokenStorage->getToken()->getUser();

        if (method_exists($this->user, 'getEnv')) {
            $this->env = $this->user->getEnv();
        }

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}
