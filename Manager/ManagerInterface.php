<?php

namespace OctopusGames\ConfigsetBundle\Manager;


interface ManagerInterface
{
    public function getClass();

    public function createObject();

    public function find(int $id);

    public function findAll();

    public function findObjectBy(array $criteria);

    public function persist($object, $flush = false);

    public function delete($object, $flush = false);

    public function flush($object = null);
}