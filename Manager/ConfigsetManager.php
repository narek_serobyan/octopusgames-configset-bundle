<?php

namespace OctopusGames\ConfigsetBundle\Manager;

use OctopusGames\ConfigsetBundle\Entity\Configset;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use OctopusGames\ConfigsetBundle\Factory\CommonFactory;
use OctopusGames\ConfigsetBundle\File\Json;
use Doctrine\Common\Persistence\ManagerRegistry;

class ConfigsetManager extends Manager
{
    use ContainerAwareTrait;

    const PRIORITY = ['version' => 10, 'platform' => 9, 'market' => 8, 'custom' => 1];

    public function __construct(ManagerRegistry $managerRegistry, $class)
    {
        parent::__construct($managerRegistry, $class);
    }

    private function generateUrl($file)
    {
        return $this->container->get('router')->generate(
            'api_get_configset_config_download',
            ['filename' => $file],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }

    public function getConfigFiles($configset, $files)
    {
        $clientFiles = $this->getChangedFiles($configset->getClientConfigs(), !empty($files['clientConfigs']) ? $files['clientConfigs'] : []);
//        $serverFiles = $this->getChangedFiles($configset->getServerConfigs(), !empty($files['serverConfigs']) ? $files['serverConfigs'] : []);
//        $all = array_merge($clientFiles, $serverFiles);

        if (!empty($clientFiles)) {
            $file = CommonFactory::makeFile($clientFiles, $this->container->getParameter('configsetUploadDir'), Json::ZIP_FILE_TYPE);
        }

        return [
            'count' => count($clientFiles),
            'file' => $this->generateUrl($file),
        ];
    }

    /**
     * @param $configs array
     * @param $files array
     * @return array
     */
    private function getChangedFiles($configs, $files)
    {
        if (!empty($files)) {
            foreach ($configs as $key => $config) {
                foreach ($files as $file) {
                    if ($file['name'] == $config['name'] && $config['md5'] == $file['md5']) {
                        unset($configs[$key]);
                    }
                }
            }
        }

        return array_column($configs, 'path');
    }

    /**
     * Filter configs collection by attirbutes
     * @param $configs
     * @param $key
     * @param $param
     * @return array
     */
    private function filterByAttr($configs, $key, $param)
    {
        $result = $resultNotEq = [];
        $key = strtolower($key);
        foreach ($configs as $id => $config) {
            $configCase = array_change_key_case(json_decode($config, true));

            if (array_key_exists($key, $configCase) && !strcasecmp($configCase[$key], $param)) {
                $result[$id] = $config;
            } elseif (!array_key_exists($key, $configCase)) {
                $resultNotEq[$id] = $config;
            }
        }

        if (empty($result)) {
            return $resultNotEq;
        }

        return $result;
    }

    /**
     *  Choose suitable config id from config ids
     * @param $configs
     * @return int|null
     */
    private function getSuitableConfig($configs)
    {
        $suitableConfig = $suitableConfigId = null;

        if (count($configs)) {
            foreach ($configs as $id => $config) {
                if (
                    ($suitableConfig && count(json_decode($config, true)) < count($suitableConfig))
                    || !$suitableConfig
                ) {
                    $suitableConfig = json_decode($config, true);
                    $suitableConfigId = $id;
                }
            }
        }

        return $suitableConfigId;
    }

    /**
     * Get configset by attributes
     * @param $params
     * @return $configset
     */
    public function getConfigsetByAttr($params)
    {
        $configs = $all = array_column($this->getRepo()->getConfigset(), 'attributes', 'id');

        $params = $this->sort($params);
        foreach($params as $key => $param) {
            $configs = $this->filterByAttr($configs, $key, $param);
        }

        $id = $this->getSuitableConfig($configs);

        return $id ? $this->find($id) : null ;
    }

    /**
     * Sorting attirbutes by priority
     * @param $params
     * @return mixed
     */
    private function sort($params)
    {
        $priority = self::PRIORITY;

        uksort($params, function ($a, $b) use ($priority) {
            if (!array_key_exists(strtolower($a), $priority)) {
                $a = 'custom';
            }
            if (!array_key_exists(strtolower($b), $priority)) {
                $b = 'custom';
            }
            if ($priority[strtolower($a)] == $priority[strtolower($b)]) {
                return 0;
            }

            return ($priority[strtolower($a)] > $priority[strtolower($b)]) ? -1 : 1;
        });

        return $params;
    }

    public function getConfigsetObj($attrs)
    {
        $data = $this->getRepo()->getConfigset(json_decode($attrs['attributes'], true), 'AND', true);
        if (!empty($data['id'])) {
            return $this->find($data['id']);
        }

        return new Configset();
    }
}
