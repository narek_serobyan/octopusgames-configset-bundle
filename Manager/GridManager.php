<?php

namespace OctopusGames\ConfigsetBundle\Manager;

use MongoRegex;
use Twig_Environment;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Response;
use OctopusGames\ConfigsetBundle\Metadata\MetadataFactory;
use OctopusGames\ConfigsetBundle\Grid\Grid;


class GridManager
{
    use ContainerAwareTrait;

    private $source;
    protected $metadata;
    protected $grid;

    public function __construct(Twig_Environment $twig, MetadataFactory $metadata) {
        $this->twig = $twig;
        $this->metadata = $metadata;
    }

    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    protected function getManager()
    {
        return $this->container->get('octopusgames_configset.configset_manager')->getManager();
    }

    private function getRequest()
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }

    private function getCurrentPage()
    {
        $requestParams = $this->getRequest()->request->all();

        return !empty($requestParams['page']) ? $requestParams['page'] : 1;
    }

    private function getDmGridData($params, $metadata)
    {
        $manager = $this->getManager()
            ->getRepository($this->source)
            ->createQueryBuilder('tmp');

        foreach($params as $key => $param) {
            if (!empty($param) && !in_array($key, ['submit', 'page', 'order'])) {
                $manager->field($key)->equals(new MongoRegex('/' .$param . '/'));
            }
        }
        $limit = $this->container->getParameter('maxPerPage');
        $offset = $limit * ($this->getCurrentPage() - 1);

        $manager = $manager->select(array_keys($metadata));

        if (!empty($params['order'])) {
            foreach ($params['order'] as $field => $type) {
                if ($type) {
                    $manager = $manager->sort($field, $type);
                }
            }
        }
        $manager = $manager->limit($limit)
            ->skip($offset)
            ->getQuery();

        return [
            'results' => $manager->toArray(),
            'count' => $manager->count(),
            'pageCount' => ceil($manager->count() / $limit)
        ];
    }

    private function qbOrder($qb, $params, $tblName)
    {
        if (!empty($params['order'])) {
            foreach ($params['order'] as $field => $type) {
                if ($type) {
                    $qb = $qb->orderBy($tblName . '.' . $field, $type);
                }
            }
        }

        return $this;
    }

    private function filterQb($qb, $params, $tblName)
    {
        foreach($params as $key => $param) {
            if (!empty($param) && !in_array($key, ['submit', 'page', 'order'])) {
                $qb->where(
                    $qb->expr()->like($tblName . '.' . $key, ':' . $key)
                )->setParameter($key, '%' . $param . '%' );
            }
        }

        return $this;
    }

    private function getQb($tblName)
    {
        return $this->getManager()
            ->getRepository($this->source)
            ->createQueryBuilder($tblName);
    }

    private function getEmGridData($params, $metadata)
    {
        $tblName = 'u';
        $limit = $this->container->getParameter('maxPerPage');
        $offset = $limit * ($this->getCurrentPage() - 1);

        $qb = $this->getQb($tblName);
        $qb->select($tblName . '.' . implode(',' . $tblName . '.', array_keys($metadata)));
        $this->filterQb($qb, $params, $tblName)
            ->qbOrder($qb, $params, $tblName);
        $result = $qb->getQuery()
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult();

        $qbCount = $this->getQb($tblName);
        $qbCount->select('count(' . $tblName . '.id)');
        $this->filterQb($qbCount, $params, $tblName);

        $count = $qbCount->getQuery()
            ->getSingleScalarResult();

        return [
            'results' => $result,
            'count' => $count,
            'pageCount' => ceil($count / $limit)
        ];
    }

    public function getGrid()
    {
        $requestParams = $this->getRequest()->request->all();
        $metadata = $this->metadata->getMetadata($this->source);
        $gridData = $this->getEmGridData($requestParams, $metadata);

        $currentRoute = $this->getRequest()->attributes->get('_route');
        $currentUrl = $this->container->get('router')->generate($currentRoute);

        $this->grid = new Grid($gridData, $metadata);
        $this->grid->setPage($this->getCurrentPage())
            ->setRouteUrl($currentUrl)
            ->setRequestParams($requestParams);

        return $this->grid;
    }


    public function renderGrid($view, $parameters = [])
    {
        $templateContent = $this->twig->loadTemplate('OctopusGamesConfigsetBundle::grid/blocks.html.twig');

        $parameters['grid'] = $templateContent->renderBlock('grid', ['grid' => $this->grid]);

        $content = $this->twig->render($view, $parameters);

        $response = new Response();
        $response->setContent($content);

        return $response;
    }
}
