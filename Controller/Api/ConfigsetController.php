<?php

namespace OctopusGames\ConfigsetBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use OctopusGames\ConfigsetBundle\Form\Factory\FactoryInterface;
use FOS\RestBundle\View\View;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Nelmio\ApiDocBundle\Annotation\Security;
use Nelmio\ApiDocBundle\Annotation\Model;
use OctopusGames\ConfigsetBundle\Entity\Configset;


/**
 * Class ConfigController
 * @package OctopusGames\ConfigsetBundle\Controller\Api
 * @Route(
 *     "/api/v1/configsets",
 *     service="octopusgames_configset.api_configset_controller"
 * )
 */
class ConfigsetController extends FOSRestController
{
    private $formFactory;

    public function __construct(FactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    private function getManager()
    {
        return $this->get('octopusgames_configset.configset_manager');
    }

    /**
     * Get and return configset by attributes
     * @Rest\Post("/configs/", name="api_get_configset_config", defaults={"_format":"json"})
     * @Rest\View()
     * @SWG\Response(
     *     response=200,
     *     description="Returns the config download url",
     * )
     * @SWG\Parameter(name="attributes", in="query", type="array", items={"name":"1.json", "md5":"f90c86d45fb54c6385a1fd03bc54387c"}, description="json")
     * @SWG\Tag(name="configsets")
     * @Security(name="Bearer")
     */
    public function getConfigAction(Request $request)
    {
        $configset = $this->getManager()->getConfigsetByAttr($request->request->get('attributes'));
        if (!$configset) {
            throw $this->createNotFoundException('The configset does not exist');
        }

        return $this->getManager()->getConfigFiles($configset, $request->request->all());
    }

    /**
     * @Rest\Get("/download/{filename}", name="api_get_configset_config_download")
     * @SWG\Response(
     *     response=200,
     *     description="Returns config files zip archive",
     * )
     * @SWG\Tag(name="configsets")
     * @Security(name="Bearer")
     */
    public function downloadAction($filename = null)
    {
        $path = $this->getParameter('configsetUploadDir');

        return $this->file($path . $filename);
    }

    /**
     * upload configsets
     * @Rest\Post("/", name="api_post_configs", defaults={"_format":"json"})
     * @Rest\View()
     * @SWG\Response(
     *     response=200,
     *     description="Returns the json of an configset",
     * )
     * @SWG\Parameter(name="configset_form['comment']", in="formData", type="string")
     * @SWG\Parameter(name="configset_form['author']", in="formData", type="string")
     * @SWG\Parameter(name="configset_form['attributes']", in="formData", type="string")
     * @SWG\Parameter(name="configset_form['serverConfigsFile']", in="formData", type="file")
     * @SWG\Parameter(name="configset_form['clientConfigsFile']", in="formData", type="file")
     * @SWG\Parameter(name="configset_form['filename']", in="formData", type="file")
     * @SWG\Tag(name="configsets")
     * @Security(name="Bearer")
     */
    public function postConfigsAction(Request $request)
    {
        $configset = $this->getManager()->getConfigsetObj($request->request->get('configset_form'));
        $form = $this->formFactory->createForm($configset);

        return $this->saveForm($form, $request);
    }

    /**
     * validate and save form
     * @param $form
     * @param $request
     * @return static
     */
    private function saveForm($form, $request)
    {
        $form->handleRequest($request);
        if ($form->isValid()) {
            $configset = $form->getData();
            $this->get('octopusgames_configset.history_manager')
                ->saveHistory($configset, $request->request->get('configset_form'));

            $this->getManager()->persist($configset, true);

            return $configset;
        } else {
            return $form;
        }
    }
}
