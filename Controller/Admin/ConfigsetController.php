<?php

namespace OctopusGames\ConfigsetBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OctopusGames\ConfigsetBundle\Form\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use APY\DataGridBundle\Grid\Source\Document;
use Gitonomy\Git\Repository;
use OctopusGames\ConfigsetBundle\Entity\Configset;

/**
 * Class ConfigController
 * @package OctopusGames\ConfigsetBundle\Controller\Api
 * @Route("/admin/configset")
 */
class ConfigsetController extends Controller
{
    private $formFactory;

    public function __construct(FactoryInterface $formFactory = null)
    {
        $this->formFactory = $formFactory;
    }

    private function getManager()
    {
        return $this->get('octopusgames_configset.configset_manager');
    }

    /**
     * view list of configs
     * @Route("/list", name="admin_configset_list")
     */
    public function listAction(request $request)
    {
        $document = $this->getParameter('configsetDocument');

        $gridService = $this->get('octopusgames_configset.grid_manager');
        $gridService->setSource($document)->getGrid()
            ->addAction('Remove', 'admin_configset_remove', ['id'=>'%id%'])
            ->addAction('View Client', 'admin_configset_view', ['id'=>'%id%', 'type'=>'client'])
            ->addAction('View Server', 'admin_configset_view', ['id'=>'%id%', 'type'=>'server'])
            ->addAction('Changes History', 'admin_configset_history', ['id'=>'%id%'])
            ->addAction('Move History', 'admin_configset_env_manager', ['id'=>'%id%']);

        return  $gridService->renderGrid(
            '@OctopusGamesConfigset/configset/admin/list.html.twig'
        );
    }

    /**
     * remove config
     * @Route("/remove/{id}", name="admin_configset_remove")
     */
    public function removeAction($id)
    {
        $configset = $this->getManager()->find($id);
        if (!$configset) {
            throw $this->createNotFoundException('The configset does not exist');
        }
        $this->getManager()->delete($configset, true);
        $this->addFlash("success", "The configset was successfully deleted");

        return $this->redirect($this->generateUrl('admin_configset_list'));
    }

    /**
     * view configs by type (server or client configs)
     * @Route("/view/{id}/{type}", name="admin_configset_view")
     */
    public function configListAction($id, $type)
    {
        $configset = $this->getManager()->find($id);
        if ($type == 'client') {
            $configs = $configset->getClientConfigs();
        } else {
            $configs = $configset->getServerConfigs();
        }

        return $this->render(
            '@OctopusGamesConfigset/configset/admin/config_list.html.twig',
            ['configs' => $configs, 'type' => $type]
        );
    }

    /**
     * download xml file
     * @Route("/download/{id}", name="admin_configset_download")
     */
    public function downloadAction($id)
    {
        $configset = $this->getManager()->find($id);
        if (!$configset) {
            throw $this->createNotFoundException('The resource does not exist');
        }

        return $this->file($configset->getFileName());
    }

    /**
     * View config upload history
     * @Route("/history/{id}", name="admin_configset_history")
     */
    public function historyAction($id, request $request)
    {
        $configset = $this->getManager()->find($id);
        if (!$configset) {
            throw $this->createNotFoundException('The resource does not exist');
        }

        $commits = $this->get('octopusgames_configset.history_manager')
            ->getHistory($configset);

        return $this->render(
            '@OctopusGamesConfigset/configset/admin/history.html.twig',
            [
                'commits' => $commits,
            ]
        );
    }

    /**
     * List of config commits by env
     * @Route("/env/manager/{id}", name="admin_configset_env_manager")
     */
    public function envManagerAction(Configset $configset, request $request)
    {
        $connections = $this->container->getParameter('doctrine.connections');

        $envData = [];
        foreach ($connections as $env =>$conn) {
            $envData[$env] = $this->get('octopusgames_configset.history_manager')
                ->getHistory($configset, $env);
        }

        return $this->render(
            '@OctopusGamesConfigset/configset/admin/envData.html.twig',
            [
                'envData' => $envData,
                'configset' => $configset
            ]
        );
    }

    /**
     * Move configs with commit history from current env to next env
     * @Route("/env/merge/", name="admin_configset_env_merge")
     */
    public function envMergeAction(request $request)
    {
        $params = $request->request->all();

        $configset = $this->getManager()->find($params['configset']);
        if (!$configset) {
            throw $this->createNotFoundException('The resource does not exist');
        }

        $comment = '<span class="btn btn-info">[' . $params['from'] . ' -> ' . $params['to'] . ']</span>';

        $comment .= PHP_EOL . $params['comment'];
        $this->get('octopusgames_configset.history_manager')
            ->mergeConfig($configset, $params['from'], $params['to'], $comment);

        return $this->redirectToRoute('admin_configset_env_manager', ['id' => $configset->getId()]);
    }

    /**
     * Export selected commit config zip archive
     * @Route("/commit/{id}/{branch}/{commit}", name="admin_get_commit_configset_config")
     */
    public function getCommitAction(Configset $configset, $branch, $commit, request $request)
    {
        $file = $this->get('octopusgames_configset.history_manager')
                ->getConfig($configset, $branch, $commit);
        $dir = $this->container->getParameter('configsetUploadDir');

        return $this->file($dir . $file);
    }
}
