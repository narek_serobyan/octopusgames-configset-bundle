'use strict'

const webpack = require('webpack')
const NODE_ENV = process.env.NODE_ENV || 'production'

module.exports = {
    context: __dirname + '/Resources/public-src/',
    entry: {
        'js/admin/main': './js/admin/main.js',
        'js/admin/config': './js/admin/config.js'
    },
    output: {
        path: __dirname + '/Resources/public/',
        publicPath: '/',
        filename: '[name].js',
        library: 'go'
    },
    devtool: NODE_ENV == 'development' ? 'cheap-inline-modules-source-map' : false,
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    plugins: ['transform-runtime'],
                    presets: ['es2015']
                }
            },
            {
                test: /\.hbs$/,
                loader: 'handlebars-loader'
            },
            {
                test: /\.sass$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    { loader: 'resolve-url-loader' },
                    { loader: 'sass-loader?sourceMap' }
                ]
            },
            {
                test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                include: /\/node_modules\/bootstrap-sass\/assets/,
                loader: 'file-loader?name=[1]&regExp=node_modules/bootstrap-sass/assets(.*)'
            },
            {
                test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                exclude: /\/node_modules\//,
                loader: 'file-loader?name=[path][name].[ext]'
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ]
}
