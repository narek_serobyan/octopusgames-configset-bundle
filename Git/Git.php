<?php

namespace OctopusGames\ConfigsetBundle\Git;

use Gitonomy\Git\Repository;
use Gitonomy\Git\Admin;
use Symfony\Component\Filesystem\Filesystem;


class Git
{
    private $init = true;
    private $repository;
    private $user;
    private $branch;

    /**
     * Git constructor.
     * @param $object
     * @param $user
     */
    public function __construct($object, $user, $branch = 'master')
    {
        $this->user = $user;
        $this->branch = $branch;
        $this->init = $object->getId() ? false : true;

        if (!$this->init) {
            $this->repository = new Repository($object->getHistoryPath());
        }
    }

    /**
     * return git init status
     * @return bool
     */
    public function getGitInit()
    {
        return $this->init;
    }

    /**
     * Initialize git repository
     * @param $dir string
     * @return $this
     */
    public function gitInit($dir)
    {
        if ($this->init) {
            Admin::init($dir, false);
        }

        $this->repository = new Repository($dir);

        return $this;
    }

    public function checkout()
    {
        $references = $this->repository->getReferences();

        if (!$references->hasBranch($this->getBranch())) {
            if ($this->getBranch() == $this->branch) {
                return $this;
            }

            $this->getWC()->checkout($this->branch, $this->getBranch());
        } else {
            $this->getWC()->checkout($this->getBranch());
        }

        return $this;
    }

    /**
     * getting working copy
     * @return mixed
     */
    public function getWC()
    {
        return $this->repository->getWorkingCopy();
    }

    /**
     * Checking is commit first
     * @return bool
     */
    public function isFirstCommit()
    {
        return !$this->repository->getReferences()->hasBranch($this->getBranch());
    }

    /**
     * staged modifications (after adding changed items git add .)
     * @return mixed
     */
    public function hasStagedModifications()
    {
        return $this->getWC()->getDiffStaged()->getRawDiff();
    }

    /**
     * create first commit for every branch
     * @param array $branches
     * @param array $params
     */
    public function createInitialCommit($branches = [], $params)
    {
        (new Filesystem())->dumpFile($this->repository->getWorkingDir() . '/readme.txt', 'init project');

        $this->setAuthor($params);
        $this->repository->run('add', ['readme.txt']);

        $this->repository->run('commit', ['-m', 'first commit']);
        foreach ($branches as $branch => $val) {
            $this->getWC()->checkout($this->branch, $branch);
        }

        $this->getWC()->checkout($this->branch);
    }


    public function setAuthor($params)
    {
        $this->repository->run('config', ['user.email', $params['author'] ?: 'example@com']);
        $this->repository->run('config', ['user.name', $params['author'] ?: 'admin']);
    }

    /**
     * commit changes
     * @param $object
     * @param array $params
     * @return $this
     */
    public function commit($object, $params = [], $branches = [])
    {
        if ($this->isFirstCommit()) {
            $this->createInitialCommit($branches, $params);
        } else {
            $this->setAuthor($params);
        }

        $this->repository->run('add', ['.']);

        if (
            $this->isFirstCommit() ||
            $this->hasStagedModifications() ||
            $this->repository->getDiff($this->getBranch())->getRawDiff()
        ) {
            $this->repository->run('commit', ['-m', $object->getComment()]);
        }

        return $this;
    }

    /**
     * return current branch
     * @return string
     */
    public function getBranch()
    {
        if (method_exists($this->user, 'getEnv')) {
            return $this->user->getEnv();
        }

        return $this->branch;
    }

    /**
     * check branch exist
     * @param $branch
     * @return mixed
     */
    public function hasBranch($branch)
    {
        return $this->repository->getReferences()->hasBranch($branch);
    }

    /**
     * merge branch to current
     * @param string $branch
     * @param string $comment
     * @return $this
     */
    public function merge(string $branch, string $comment)
    {
        $this->repository->run('merge', [$branch, '--commit', '-m', $comment, '--no-ff']);

        return $this;
    }

    /**
     * get git commits list
     * @param string $branch
     * @return array
     */
    public function getCommits(string $branch = 'master')
    {
        if (!$this->repository->getReferences()->hasBranch($branch)) {
            return [];
        }

        return $this->repository->getLog($branch)->getCommits();
    }
}