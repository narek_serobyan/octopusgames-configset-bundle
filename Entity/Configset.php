<?php

namespace OctopusGames\ConfigsetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OctopusGames\ConfigsetBundle\Entity\Model\ConfigsetBase;
use OctopusGames\ConfigsetBundle\Annotation as Octo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="configset")
 * @ORM\Entity(
 *     repositoryClass="OctopusGames\ConfigsetBundle\Repository\ConfigsetEntityRepository"
 * )
 * @Octo\Grid(columns="id, comment, attributes, filename, createdAt")
 * @Octo\History(columns="comment, attributes, filename, clientConfigs, serverConfigs, historyPath")
 */
class Configset extends ConfigsetBase
{
}
