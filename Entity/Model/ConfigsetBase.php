<?php

namespace OctopusGames\ConfigsetBundle\Entity\Model;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Events;
use Symfony\Component\Filesystem\Filesystem;
use OctopusGames\ConfigsetBundle\Annotation as Octo;
use OctopusGames\ConfigsetBundle\File\File as OctoFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class ConfigsetBase
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Octo\GridColumn(title="comment", type="string", filterable=true)
     * @Octo\HistoryColumn(title="Comment", type="string")
     */
    protected $comment = null;

    /**
     * @ORM\Column(type="json_document", options={"jsonb": true})
     * @Octo\GridColumn(title="Attributes", type="array", filterable=true)
     * @Octo\HistoryColumn(title="Attributes", type="string_array")
     */
    protected $attributes;

    /**
     * @ORM\Column(type="date")
     * @Octo\GridColumn(title="Created At", type="date", filterable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string")
     * @Octo\GridColumn(title="Filename", type="file", filterable=false)
     * @Octo\HistoryColumn(title="Filename", type="file")
     */
    protected $filename;

    /**
     * @ORM\Column(type="json_document", options={"jsonb": true})
     * @Octo\HistoryColumn(title="clientConfigs", type="array")
     */
    protected $clientConfigs;

    /**
     * @ORM\Column(type="json_document", options={"jsonb": true})
     * @Octo\HistoryColumn(title="serverConfigs", type="array")
     */
    protected $serverConfigs;

    /**
     * @ORM\Column(type="string")
     * @Octo\HistoryColumn(title="Hitory Path", type="string")
     */
    protected $historyPath;

    /**
     */
    protected $file;

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set attributes
     *
     * @param hash $attributes
     * @return $this
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Get attributes
     *
     * @return hash $attributes
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set createdAt
     * @ORM\PrePersist
     * @param date $createdAt
     * @return $this
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return date $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set fileName
     * @param custom_id $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->filename = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return custom_id $fileName
     */
    public function getFileName()
    {
        return $this->filename;
    }


    /**
     * Set clientConfigs.
     *
     * @param array $clientConfigs
     *
     * @return Configset
     */
    public function setClientConfigs($clientConfigs)
    {
        $this->clientConfigs = $clientConfigs;

        return $this;
    }

    /**
     * Get clientConfigs.
     *
     * @return array
     */
    public function getClientConfigs()
    {
        return $this->clientConfigs ?: [];
    }

    /**
     * Set serverConfigs.
     *
     * @param array $serverConfigs
     *
     * @return Configset
     */
    public function setServerConfigs($serverConfigs)
    {
        $this->serverConfigs = $serverConfigs;

        return $this;
    }

    /**
     * Get serverConfigs.
     *
     * @return array
     */
    public function getServerConfigs()
    {
        return $this->serverConfigs ?: [];
    }

    /**
     * Set historyPath
     *
     * @param historyPath $historyPath
     * @return $this
     */
    public function setHistoryPath($historyPath)
    {
        $this->historyPath = $historyPath;

        return $this;
    }

    /**
     * Get historyPath
     *
     * @return historyPath $historyPath
     */
    public function getHistoryPath()
    {
        return $this->historyPath;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ORM\PreRemove
     */
    public function deleteHistory()
    {
        if ($this->getClientConfigs()) {
            (new Filesystem())->remove(OctoFile::getBasePath($this->getClientConfigs()[0]['path'], 3));
        }
        if ($this->getServerConfigs()) {
            (new Filesystem())->remove(OctoFile::getBasePath($this->getServerConfigs()[0]['path'], 3));
        }
        if ($this->getFileName()) {
            (new Filesystem())->remove(OctoFile::getBasePath($this->getFileName(), 2));
        }

        (new Filesystem())->remove($this->getHistoryPath());
    }

    /**
     * @ORM\PreUpdate
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        if ($event->hasChangedField('filename')) {
            (new Filesystem())->remove(OctoFile::getBasePath($event->getEntityChangeSet()['filename'][0], 2));
        }
    }
}
