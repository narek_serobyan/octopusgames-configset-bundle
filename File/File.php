<?php

namespace OctopusGames\ConfigsetBundle\File;

use Symfony\Component\Filesystem\Filesystem;

class File
{
    public static function saveFile($file, $dir)
    {
        $uploadName = strval($file->getClientOriginalName());
        $dir = substr($dir, -1) == '/' ? $dir : $dir . '/';

        $fileSystem = new Filesystem();
        $fileSystem->mkdir($dir);

        $file->move($dir, $uploadName);

        return [
            'path' => $dir . $uploadName,
            'fileName' => $uploadName
        ];
    }

    public static function getBasePath($path, $deep = 1)
    {
        $items = explode('/', $path);
        for ($i=1; $i < $deep; $i++) {
            array_pop($items);
        }

        return implode('/', $items);
    }
}