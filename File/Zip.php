<?php

namespace OctopusGames\ConfigsetBundle\File;

use ZipArchive;
use  OctopusGames\ConfigsetBundle\File\FileGenerateInterface;

class Zip implements FileGenerateInterface
{
    /**
     * @param $files
     * @param $path
     * @return string
     */
    public function generateFile($files, $path)
    {
        $zip = new ZipArchive();
        $zipName = 'configs_' . time() . ".zip";
        $zip->open($path . $zipName, \ZipArchive::CREATE);
        foreach ($files as $f) {
            $zip->addFromString(basename($f), file_get_contents($f));
        }
        $zip->close();

        return $zipName;
    }


}