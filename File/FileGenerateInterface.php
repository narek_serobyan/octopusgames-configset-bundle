<?php

namespace OctopusGames\ConfigsetBundle\File;


Interface FileGenerateInterface
{
    const JSON_FILE_TYPE = 'JSON';
    const ZIP_FILE_TYPE = 'ZIP';

    public function generateFile($files, $path);

}