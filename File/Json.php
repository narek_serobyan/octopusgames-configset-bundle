<?php

namespace OctopusGames\ConfigsetBundle\File;

use  OctopusGames\ConfigsetBundle\File\FileGenerateInterface;


class Json implements FileGenerateInterface
{
    /**
     * @param $files
     * @param $path
     * @return string
     */
    public function generateFile($files, $path)
    {
        $json = [];
        foreach ($files as $file) {
            $fileName = explode('/', $file);
            $json[end($fileName)] = json_decode(file_get_contents($file), true);
        }

        return $json;
    }


}