<?php

namespace OctopusGames\ConfigsetBundle\Metadata;

use OctopusGames\ConfigsetBundle\Metadata\DriverInterface;

class MetadataFactory
{
    protected $driver;

    public function __construct(DriverInterface $driver)
    {
        $this->driver = $driver;
    }

    public function getMetadata($className, $group = 'default')
    {
        $columns = $this->driver->getClassColumns($className, $group);
        $fieldsMetadata[] = $this->driver->getFieldsMetadata($className, $group);

        $mappings = $cols = [];

        foreach ($columns as $fieldName) {
            $map = [];

            foreach ($fieldsMetadata as $field) {
                if (isset($field[$fieldName]) && (!isset($field[$fieldName]['groups']) || in_array(
                            $group,
                            (array)$field[$fieldName]['groups']
                        ))) {
                    $map = array_merge($map, $field[$fieldName]);
                }
            }

            if (!empty($map)) {
                $mappings[$fieldName] = $map;
                $cols[] = $fieldName;
            }
        }

        return $mappings;
    }
}