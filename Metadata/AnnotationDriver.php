<?php

namespace OctopusGames\ConfigsetBundle\Metadata;

use Doctrine\Common\Annotations\Reader;

class AnnotationDriver implements DriverInterface
{
    protected $columns;
    protected $fields;
    protected $reader;

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    public function getClassColumns($class, $group = 'default')
    {
        $this->loadMetadataFromReader($class, $group);

        return $this->columns[$class][$group];
    }

    public function getFieldsMetadata($class, $group = 'default')
    {
        $this->loadMetadataFromReader($class, $group);

        return $this->fields[$class][$group];
    }
}