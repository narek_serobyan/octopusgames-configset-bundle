<?php

namespace OctopusGames\ConfigsetBundle\Metadata;

interface DriverInterface
{
    public function getClassColumns($class, $group = 'default');

    public function getFieldsMetadata($class, $group = 'default');
}
