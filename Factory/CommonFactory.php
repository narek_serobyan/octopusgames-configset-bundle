<?php

namespace OctopusGames\ConfigsetBundle\Factory;

use OctopusGames\ConfigsetBundle\File\Zip;
use OctopusGames\ConfigsetBundle\File\Json;
use  OctopusGames\ConfigsetBundle\File\FileGenerateInterface;

class CommonFactory
{
    public static function makeFile($all, $path, $type)
    {
        switch ($type){
            case FileGenerateInterface::ZIP_FILE_TYPE :
                $file = (new Zip)->generateFile($all, $path);
                break;
            case FileGenerateInterface::JSON_FILE_TYPE:
                $file = (new Json)->generateFile($all, $path);
        }

        return $file;
    }
}