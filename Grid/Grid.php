<?php

namespace OctopusGames\ConfigsetBundle\Grid;

class Grid
{
    private $titleSectionVisible = true;
    private $filterSectionVisible = true;
    private $pagerSectionVisible = true;

    private $data;
    private $rows;
    private $columns;

    private $page;
    private $pageCount;
    private $totalCount;

    private $routeUrl;
    private $requestParams;
    private $actionsTitle = 'Actions';
    private $actions = [];

    /**
     * Grid constructor.
     */
    public function __construct($data, array $columns = [])
    {
        $this->data = $data;
        $this->columns = $columns;
        $this->rows = $data['results'];
        $this->rows = $data['results'];
        $this->pageCount = $data['pageCount'];
        $this->totalCount = $data['count'];
    }

    public function isTitleSectionVisible()
    {
        return $this->titleSectionVisible;
    }

    public function setTitleSectionVisible($titleSectionVisible)
    {
        $this->titleSectionVisible = $titleSectionVisible;

        return $this;
    }

    public function isFilterSectionVisible()
    {
        return $this->filterSectionVisible;
    }

    public function setFilterSectionVisible($filterSectionVisible)
    {
        $this->filterSectionVisible = $filterSectionVisible;

        return $this;
    }

    public function isPagerSectionVisible()
    {
        return $this->pagerSectionVisible;
    }

    public function setPagerSectionVisible($pagerSectionVisible)
    {
        $this->pagerSectionVisible = $pagerSectionVisible;

        return $this;
    }

    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getRows()
    {
        return $this->rows;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function getPageCount()
    {
        return $this->pageCount;
    }

    public function getTotalCount()
    {
        return $this->totalCount;
    }

    public function getHash()
    {
        return md5(time());
    }

    public function setRouteUrl(string $routeUrl)
    {
        $this->routeUrl = $routeUrl;

        return $this;
    }

    public function getRouteUrl()
    {
        return $this->routeUrl;
    }

    public function setRequestParams(array $requestParams)
    {
        $this->requestParams = $requestParams;

        return $this;
    }

    public function getRequestParams()
    {
        return $this->requestParams;
    }

    public function addAction(string $name, string $route, array $params)
    {
        $this->actions[] = ['name' => $name, 'route' => $route, 'params' => $params];

        return $this;
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function getActionsTitle()
    {
        return $this->actionsTitle;
    }

    public function setActionsTitle(string $title)
    {
        $this->actionsTitle = $title;

        return $this;
    }
}